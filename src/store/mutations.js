export default {
  // species and pet are coming from the given payload, see actions
  appendPet: (state, { species, pet }) => {
    state[species].push(pet)
  }
}
