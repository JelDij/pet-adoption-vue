export default {
  // commit comes from vuex, its a shortcut for (context, pet) as parameters and context.commit() in the method
  addPet: ({ commit }, payload) => {
    commit('appendPet', payload)
  }
}
